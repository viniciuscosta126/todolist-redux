import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container} from 'react-bootstrap'
import AddTask from './components/AddTask';
import Tasks from './components/Tasks';

function App() {
  return (
    <div className="App">
      <Container>
        <AddTask/>
        <Tasks/>
      </Container>
    </div>
  );
}

export default App;
