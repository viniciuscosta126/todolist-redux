import React from "react";
import { ListGroup, Button, Row, Col } from "react-bootstrap";
import { selectTask, removeTask, checkedTask } from "../Redux/taskSlicer";
import { useSelector, useDispatch } from "react-redux";
const Tasks = () => {
  const { tasks } = useSelector(selectTask);
  const dispatch = useDispatch();
  const handleClick = (taskID) => {
    dispatch(removeTask(taskID));
  };
  const check = (taskID) => {
    dispatch(checkedTask(taskID));
  };

  if (!tasks.length) {
    return <h1>Sem tasks</h1>;
  } else {
    return (
      <ListGroup>
        <Row className="align-itens-center" className="my-1">
          {tasks.map((tarefa) => {
            return (
              <>
                <Col sm={8} key={tarefa.id} className="my-1">
                  <ListGroup.Item
                    action
                    variant={tarefa.isCompleted ? "success" : ""}
                  >
                    {tarefa.task}{" "}
                  </ListGroup.Item>
                </Col>
                <Col sm={1} className="my-1">
                  <Button
                    variant="success"
                    className=""
                    onClick={() => check(tarefa.id)}
                  >
                    ✔
                  </Button>
                </Col>
                <Col sm={1} className="my-1">
                  <Button
                    variant="danger"
                    onClick={() => handleClick(tarefa.id)}
                  >
                    X
                  </Button>
                </Col>
              </>
            );
          })}
        </Row>
      </ListGroup>
    );
  }
};
export default Tasks;
