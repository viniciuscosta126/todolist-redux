import React, { useState } from "react";
import { Button, Form, Row, Col } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { addTask } from "../Redux/taskSlicer";

const AddTask = () => {
  const [task, setTask] = useState("");
  const dispatch = useDispatch();

  const handleClick = () => {
    dispatch(addTask(task));
  };
  return (
    <div>
      <Form>
        <Row className="align-itens-center my-5">
          <Col sm={10}>
            <Form.Control
              type="text"
              placeholder="Digite a task"
              value={task}
              onChange={(e) => setTask(e.target.value)}
            />
          </Col>
          <Col sm={2}>
            <Button onClick={handleClick}>Enviar</Button>
          </Col>
        </Row>
      </Form>
    </div>
  );
};

export default AddTask;
