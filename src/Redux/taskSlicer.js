import { createSlice } from "@reduxjs/toolkit";

export const slice = createSlice({
  name: "task",
  initialState: [
  ],
  reducers: {
    addTask(state, { payload }) {
      return [...state, { id: Math.floor(Math.random() * (20000 - 1 + 1)) + 1, task: payload, isCompleted: false }];
    },
    removeTask(state, { payload }) {
        return state.filter((item) => item.id !== payload);
    },
    checkedTask(state, { payload }) {
      const newTasks = state.map((item) => {
        if (item.id === payload) return { ...item, isCompleted: !item.isCompleted };
        return item;
      })
      return (
        newTasks
      )
    }
  },
});

export const { addTask,removeTask,checkedTask } = slice.actions;
export const selectTask = (state) =>state
export default slice.reducer;
