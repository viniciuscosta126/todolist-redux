import { configureStore } from "@reduxjs/toolkit";
import taskReducer from "./taskSlicer";

export default configureStore({
  reducer: {
    tasks: taskReducer,
  },
});
